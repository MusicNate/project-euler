def sieve_upto(top)
	sieve = []
	# Put all the numbers in an array
	for i in 2 .. top do
		sieve[i] = i
	end
	for i in 2 .. Math.sqrt(top)
		next unless sieve[i]
		(i*i).step(top, i) do |j|
			sieve[j] = nil
		end
	end
	sieve.compact
end

def sum(sieve)
	total = 0
	sieve.each do |x|
		total += x
	end
	puts total
end

sum(sieve_upto(2000000))