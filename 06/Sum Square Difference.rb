square_sum = 0
sum_square = 0

# Find square of sums
addition = 0
for i in 1..100
    addition += i
end
square_sum = addition**2

# Find sum of squares
squares = 0
for i in 1..100
    squares += i**2
end

diff = square_sum - squares
puts diff
