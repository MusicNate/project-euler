prime_numbers = [2, 3, 5, 7, 11]
i = 11

while prime_numbers.length < 10001
    count = 0
    for k in prime_numbers    
        if i % k >= 1
            count += 1 # counting through all of the prime numbers so far
        elsif i % k == 0
            break
        end
        if count == prime_numbers.length  # if current iteration is not divisible by any of the previous prime_numbers
            prime_numbers.push(i)  # adds the new prime to the array
        end
    end
    i += 1
end
puts prime_numbers.last
